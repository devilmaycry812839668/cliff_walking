# -*- coding: utf-8 -*-
"""
Created on Mon Jul 20 20:10:53 2020

@author: 81283
"""

from environment.env import x_scope, y_scope, actions


#动作值的字典,key为坐标和动作：((x,y),action) , value为q值
q_dict=dict()


# 动作值字典初始化，每次实验前都需清空
def q_value_init():
    q_dict.clear() #q_value 字典清空
    for i in range(x_scope):
        for j in range(y_scope):
            for action in actions:
                q_dict[((i, j), action)]=0 


