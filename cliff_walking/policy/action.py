# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 10:07:55 2020

@author: 81283
"""


import random
from environment.env import actions   # 环境中agent可选动作列表
from .value import q_dict   # 动作值的字典,key为坐标和动作：((x,y),action) , value为q值


# 探索概率 0.1  超参数
epsilon=0.1


# 动作选择法
def _action(state, act_str):
    q_value_list=[]
    for action in actions:
        q_value_list.append( (action, q_dict[(state, action)]) ) 
    
    if act_str=="greedy":
        random.shuffle(q_value_list)
        q_value_list.sort(key=lambda x:x[1])
        # 返回q值最大的  tuple(动作action， q值)
        return q_value_list[-1] 
    elif act_str=="random":
        # 随机返回  tuple(动作action， q值)
        return random.choice(q_value_list)
    
    
# greedy策略,最大动作值选择法
def greedy_action(state):
    return _action(state, "greedy")


# epsilon-greedy策略动作选择法
def epsilon_action(state):
    if random.random()>epsilon:
        return _action(state, "greedy")
    else:
        return _action(state, "random")