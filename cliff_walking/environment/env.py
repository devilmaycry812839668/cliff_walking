# -*- coding: utf-8 -*-
"""
Created on Mon Jul 20 17:08:16 2020

@author: 81283
"""


#横坐标范围 12， 纵坐标范围 4
x_scope=12
y_scope=4


#动作的选择为上，下，左, 右
actions=["up", "down", "left", "right"]


# 状态state为tuple，（x,y）状态所处的横纵坐标
# action 可为: up、down、left、right
# return 当前状态选择动作后的： 三元组：（下一状态坐标，奖励，是否结束）
def next_state_reward_end(state, action, no_cliff=False):
    """  初始状态坐标（0,0），终止状态坐标（x_scope-1, 0）"""
    # 现状态坐标 x, y  下一状态坐标 next_x, next_y
    x, y=state
    
    # 返回状态是否为结束状态
    end_flag=False
    
    if action=="up":
        next_x=x
        next_y=y+1
    elif action=="down":
        next_x=x
        next_y=y-1
    elif action=="left":
        next_x=x-1
        next_y=y
    elif action=="right":
        next_x=x+1
        next_y=y


    # 判断下一状态是否为终止状态
    if next_x==x_scope-1 and next_y==0:
        reward=-1
        end_flag=True
        #进入终止状态
        return (next_x, next_y), reward, end_flag


    # 撞墙判断
    # 如果下一个状态为撞墙为定义为跳回原状态，reward为 -1
    # 初始状态为（0,0），终止状态：（x_scope-1, 0）
    if next_x<0 or next_x>(x_scope-1) or next_y<0 or next_y>(y_scope-1):
        next_x, next_y=x, y          # 跳转回原状态
        reward=-1 
        end_flag=False
        # 跳回原状态，回报值为 -1
        return (next_x, next_y), reward, end_flag


    # 有悬崖设定
    if no_cliff==False:
        # 悬崖判断
        # 进入到悬崖，跳转回初始状态，回报为 -100
        if 0<next_x<(x_scope-1) and next_y==0:
            next_x, next_y=0, 0          # 跳转回初始状态
            reward=-100
            end_flag=False
            return (next_x, next_y), reward, end_flag


    # 普通状态判断
    reward=-1
    end_flag=False
    return (next_x, next_y), reward, end_flag


